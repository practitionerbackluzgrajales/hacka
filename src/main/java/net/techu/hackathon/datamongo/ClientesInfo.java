package net.techu.hackathon.datamongo;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;

@Document("clientesInfo")
public class ClientesInfo {

    public String id;
    public String nombreCompleto;
    public String documento;
    public String ciudad;
    public ArrayList<Productos> productos;

    public ClientesInfo(String nombreCompleto, String ciudad, String documento,ArrayList<Productos> productos) {
        this.id = id;
        this.nombreCompleto = nombreCompleto;
        this.documento = documento;
        this.ciudad = ciudad;
        this.productos = productos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public ArrayList<Productos> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Productos> productos) {
        this.productos = productos;
    }

    @Override
    public String toString() {
        return "ClientesInfo{" +
                "id='" + id + '\'' +
                ", nombreCompleto='" + nombreCompleto + '\'' +
                ", documento='" + documento + '\'' +
                ", ciudad='" + ciudad + '\'' +
                ", productos=" + productos +
                '}';
    }
}
