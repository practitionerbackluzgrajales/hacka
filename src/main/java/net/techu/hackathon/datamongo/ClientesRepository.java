package net.techu.hackathon.datamongo;


import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientesRepository extends MongoRepository<ClientesInfo, String>{

    @Query("{$and:[{_id: ObjectId('?0') }, {'productos.idProducto':'?1'}]}")
    public List<ClientesInfo> findByProductoCliente(String id, String codigoProducto);

    @Query("{'documento':'?0'}")
    public ClientesInfo findByByDocument(String id);

}








