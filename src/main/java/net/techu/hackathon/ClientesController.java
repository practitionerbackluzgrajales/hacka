package net.techu.hackathon;

import net.techu.hackathon.datamongo.ClientesInfo;
import net.techu.hackathon.datamongo.ClientesRepository;
import net.techu.hackathon.datamongo.Productos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClientesController {

    @Autowired
    private ClientesRepository repository;

    // Get lista de clientesInfo
    @GetMapping(value = "/v1/clientesproductos", produces = "application/json")
    public ResponseEntity<List<ClientesInfo>> obtenerListadoClientesInfo()
    {
        List<ClientesInfo> lista = repository.findAll();
        return new ResponseEntity<List<ClientesInfo>>(lista, HttpStatus.OK);
    }

    // Get un clientesInfo
    @GetMapping(value = "/v1/clientesproductos/{id}", produces = "application/json")
    public ResponseEntity<ClientesInfo> obtenerClienteInfo(@PathVariable String id)
    {
        ClientesInfo clientesInfo = null;
        try {
            clientesInfo = repository.findById(id).get();
            return new ResponseEntity<ClientesInfo>(clientesInfo, HttpStatus.OK);

        }
        catch(Exception exception) {
            return new ResponseEntity<ClientesInfo>(clientesInfo, HttpStatus.NOT_FOUND);
        }
    }

    // Get by documento
    @GetMapping(value = "/v1/clientesbyid/{id}", produces = "application/json")
    public ResponseEntity<ClientesInfo> obtenerClienteByDocumento(@PathVariable String id)
    {
        ClientesInfo clientesInfo = null;
        try {
            clientesInfo = repository.findByByDocument(id);
            return new ResponseEntity<ClientesInfo>(clientesInfo, HttpStatus.OK);

        }
        catch(Exception exception) {
            return new ResponseEntity<ClientesInfo>(clientesInfo, HttpStatus.NOT_FOUND);
        }
    }


    //Post Clientes Info
    @PostMapping(value="/v1/clientesproductos", produces = "application/json")
    public ResponseEntity<String> addCliente(@RequestBody ClientesInfo clientesInfo)
    {
        ClientesInfo resultado = repository.insert(clientesInfo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }


    //Put
    @PutMapping(value="/v1/clientesproductos/{id}")
    public ResponseEntity<String> updateClientesInfo(@PathVariable String id, @RequestBody ClientesInfo clientesInfo){
        if (repository.findById(id).isPresent()){
            ClientesInfo resultado = repository.findById(id).get();
            resultado.setNombreCompleto(clientesInfo.getNombreCompleto());
            resultado.setDocumento(clientesInfo.getDocumento());
            resultado.setCiudad(clientesInfo.getCiudad());
            resultado.setProductos (clientesInfo.getProductos());
            repository.save(resultado);
            return new ResponseEntity<>("Cliente modificado", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Cliente no modificado", HttpStatus.NOT_FOUND);
    }

    //Put
    @PutMapping(value="/v1/clientesproductos/{id}/product")
    public ResponseEntity<String> addProductoCliente(@PathVariable String id, @RequestBody Productos productos){
        if (repository.findById(id).isPresent()){
            ClientesInfo resultado = repository.findById(id).get();
            resultado.getProductos().add(productos);
            repository.save(resultado);
            return new ResponseEntity<>("Cliente modificado", HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>("Cliente no modificado", HttpStatus.NOT_FOUND);
    }

    //Patch
    @PatchMapping(value="/v1/clientesproductos/{id}/{codigoproducto}")
    public ResponseEntity<String> updateProductoClientesInfo(@PathVariable String id,@PathVariable String codigoproducto, @RequestBody Productos productocliente){
        int indice = 0;
        if (!repository.findById(id).isPresent()){
            return new ResponseEntity<>("Cliente no existe", HttpStatus.NOT_FOUND);
        }
        if (repository.findByProductoCliente(id, codigoproducto)== null){
            return new ResponseEntity<>("Cliente no tiene producto", HttpStatus.NOT_FOUND);
        }
        ClientesInfo resultado = repository.findById(id).get();
        for (Productos producto: resultado.getProductos()) {
            if (producto.getIdProducto().equals(codigoproducto)){
                producto.setSaldo(productocliente.getSaldo());
                repository.save(resultado);
            }
        }
        return new ResponseEntity<>("Saldo de producto modificado", HttpStatus.NO_CONTENT);
    }
}
